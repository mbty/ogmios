#include <iostream>
#include <functional>

#include "../src/ogmios.hh"

int main() {
  ogmios::graph graph;

  std::function<void ()> say_bonjour = [] { std::cout << "bonjour\n"; };
  std::function<void ()> say_hello   = [] { std::cout << "hello\n"  ; };
  std::function<void ()> say_hallo   = [] { std::cout << "hallo\n"  ; };

  auto id1 = graph.add_node(say_hello);
  auto id2 = graph.add_node(say_hello);

  graph.add_node(say_hello);
  graph.add_node(say_bonjour);
  graph.add_node(say_bonjour);
  graph.add_node(say_bonjour);
  graph.add_node(say_hallo, {id1, id2});

  graph.execute();
}
