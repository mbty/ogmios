#include "ogmios.hh"

namespace ogmios {
  void graph::execute() {
    std::condition_variable task_over_notification;
    std::condition_variable end_thread_request;

    std::mutex task_over_mutex;
    std::mutex inter_thread_mutex;
    std::mutex end_thread_mutex;

    size_t over_node = 0;

    std::unique_lock<std::mutex> task_over_lock(task_over_mutex);
    std::unique_lock<std::mutex> end_thread_lock(
      end_thread_mutex, std::defer_lock
    );

    std::set<size_t> running_nodes(start_nodes.begin(), start_nodes.end());
    for (size_t id : running_nodes) {
      nodes[id-1].execute_async(
        task_over_notification, task_over_mutex, end_thread_request,
        end_thread_mutex, inter_thread_mutex, over_node
      );
    }

    std::map<size_t, size_t> nodes_to_parents_count_map;
    for (size_t i = 0; i < nodes.size(); ++i) {
      nodes_to_parents_count_map.insert(
        {i + 1, nodes[i].get_parents_count()}
      );
    }

    while (!(running_nodes.empty())) {
      // wait automatically unlocks the mutex, and relocks it on wakeup
      task_over_notification.wait(
        task_over_lock, [&over_node](){ return (over_node != 0); }
      );

      // Remove over_node from running
      running_nodes.erase(running_nodes.find(over_node));

      // Start children when ready
      for (size_t child_id : nodes[over_node-1].get_children()) {
        --nodes_to_parents_count_map[child_id];
        if (nodes_to_parents_count_map[child_id] == 0) {
          running_nodes.insert(child_id);
          nodes[child_id-1].execute_async(
            task_over_notification, task_over_mutex, end_thread_request,
            end_thread_mutex, inter_thread_mutex, over_node
          );
        }
      }

      // Reset to nullptr for synchronization purposes
      end_thread_lock.lock();
      over_node = 0;

      // Synchronize
      end_thread_lock.unlock();
      end_thread_request.notify_one();
    }

    std::lock_guard<std::mutex> lock_threads(inter_thread_mutex);
  }

  size_t graph::add_node(std::function<void()> task) {
    nodes.emplace_back(task, 0);
    start_nodes.emplace_back(nodes.size());
    return nodes.size();
  }

  size_t graph::add_node(std::function<void()> task, size_t parent) {
    nodes.emplace_back(task, 1);
    nodes[parent - 1].register_child(nodes.size());
    return nodes.size();
  }

  size_t graph::add_node(
    std::function<void()> task, std::vector<size_t> parents
  ){
    nodes.emplace_back(task, parents.size());
    for (size_t parent : parents) {
      nodes[parent - 1].register_child(nodes.size());
    }
    return nodes.size();
  }

  graph::graph()
  : nodes()
  { }
}
