#include "ogmios.hh"
namespace ogmios {
  size_t graph::node::next_id = 1;

  graph::node::node(std::function<void()> _task, size_t _parents_count)
  : id(next_id++), task(_task), parents_count(_parents_count) { }

  void graph::node::register_child(size_t child) {
    children.push_back(child);
  }

  void graph::node::execute_async(
    std::condition_variable &task_over_notification,
    std::mutex &task_over_mutex, std::condition_variable &end_thread_request,
    std::mutex &end_thread_mutex, std::mutex &inter_thread_mutex,
    size_t &over_node
  ) {
    std::thread executor(
      &node::execute, this, std::ref(task_over_notification),
      std::ref(task_over_mutex), std::ref(end_thread_request),
      std::ref(end_thread_mutex), std::ref(inter_thread_mutex),
      std::ref(over_node)
    );
    executor.detach();
  }

  void graph::node::execute(
    std::condition_variable &task_over_notification,
    std::mutex &task_over_mutex, std::condition_variable &end_thread_request,
    std::mutex &end_thread_mutex, std::mutex &inter_thread_mutex,
    size_t &over_node
  ) {
    task();

    std::lock_guard<std::mutex> inter_thread_lock(inter_thread_mutex);
    std::unique_lock<std::mutex> end_thread_lock(end_thread_mutex);
    std::unique_lock<std::mutex> task_over_lock(task_over_mutex);
    task_over_lock.unlock();

    over_node = id;
    task_over_notification.notify_one();

    end_thread_request.wait(
      end_thread_lock, [&over_node](){ return (over_node == 0); }
    );
  }

  const std::vector<size_t> &graph::node::get_children() const {
    return children;
  }

  size_t graph::node::get_parents_count() const {
    return parents_count;
  }
}
