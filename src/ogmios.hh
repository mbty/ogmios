#ifndef OGMIOS_HH
#define OGMIOS_HH

#include <condition_variable>
#include <chrono>
#include <exception>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <thread>
#include <vector>

namespace ogmios {
  class graph {
    public:
      graph();

      void execute();

      size_t add_node(std::function<void()> task);
      size_t add_node(std::function<void()> task, size_t parent);
      size_t add_node(std::function<void()> task, std::vector<size_t> parents);

    private:
      class node {
        public:
          node(std::function<void()> _task, size_t _parents_count);

          void register_child(size_t child);

          void execute_async(
            std::condition_variable &task_over_notification,
            std::mutex &task_over_mutex,
            std::condition_variable &end_thread_request,
            std::mutex &end_thread_mutex, std::mutex &inter_thread_mutex,
            size_t &over_node
          );

          const std::vector<size_t> &get_children() const;
          size_t get_parents_count() const;

        private:
          static size_t next_id;
          size_t id;

          std::function<void()> task;

          std::vector<size_t> children;
          unsigned int parents_count;

          void execute(
            std::condition_variable &task_over_notification,
            std::mutex &task_over_mutex,
            std::condition_variable &end_thread_request,
            std::mutex &end_thread_mutex, std::mutex &inter_thread_mutex,
            size_t &over_node
          );
      };

    std::vector<node> nodes;
    std::vector<size_t> start_nodes;
  };
}

#endif
